<?php

namespace aqsat_integration_bnpl\postpay\providers;

use aqsat_integration_bnpl\postpay\api\Main;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider{

    public function register() {

        if($this->app->runningInConsole()){

            $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

            $this->commands([  ]);
        }


        $this->registerConfig();

    }


    public function boot() {

        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'postpay');

    }

    protected function registerConfig(){

        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('Postpay.php'),
        ], 'config');

        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'postpay'
        );
        $this->app->singleton('Postpay', static function () {

            return new Main();
        });
    }
}
