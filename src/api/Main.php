<?php

/*
 * @author Anas almasri (anas.almasri@hayperpay.com)
 */

namespace aqsat_integration_bnpl\postpay\api;

class Main{


    final public static function postpay(): Postpay
    {

        return Postpay::object(false);

    }

}
