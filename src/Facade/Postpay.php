<?php

/*
 * @author Anas almasri (anas.almasri@hayperpay.com)
 * @Description: This is facade class for Helper packages
 */


namespace aqsat_integration_bnpl\postpay\Facade;


use Illuminate\Support\Facades\Facade;

/**
 * @method static \aqsat_integration_bnpl\postpay\api\Postpay postpay(string $debug = false, int $time_out = 30)
 */

class Postpay extends Facade{

    /***************************************************************
     *
     * @Original_Author: Anas almasri (anas.almasri@hayperpay.com)
     * @Description: Get the registered name of the component.
     *
     ***************************************************************
     */
    protected static function getFacadeAccessor(){

        return 'Postpay';
    }
}
