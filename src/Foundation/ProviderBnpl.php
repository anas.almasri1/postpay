<?php

namespace aqsat_integration_bnpl\postpay\Foundation;

use aqsat\bnpl\Interface\BnplInterface;
use aqsat\orders\Constants\OrderStatus;
use GuzzleHttp\Exception\ClientException;
use aqsat_integration_bnpl\postpay\Facade\Postpay;



class ProviderBnpl implements BnplInterface{


    public function getOffers(){

        try {

            $response = Postpay::postpay()->offer();

            //$response =  json_decode($response->getBody()->getContents() , true);

            sleep(1);

            return $response;

        }catch (ClientException $exception){

            return $exception->getMessage(); // todo add logs

        }
    }

    public function bnplForm(){

        return Validation::bnplForm();

    }

    public function bnplFormValidation(){

        return Validation::bnplFormValidation();

    }

     public function payment()
    {
        sleep(1);

        return [
            'status'=> OrderStatus::COMPLETED,
            'messages'=>'Payment Success',
            'amount'=>401
        ];

        try {
            $response = Postpay::postpay()->paymentOffer();

            //$response =  json_decode($response->getBody()->getContents() , true);
            sleep(1);

            return $response;

        }catch (ClientException $exception){

            return $exception->getMessage(); // todo add logs

        }
    }


    public function agreementPage(){
        return trans('postpay::response.agreement_page');

    }
}
